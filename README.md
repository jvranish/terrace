# Terrace #

This is still a work in progress, not quite ready for general use yet. 

This is pretty much an example project at the moment, stolen from the CMock examples. 

You will need ninja installed to run the examples.

###The core terrace files are:

- `terrace.rb`
- `ninja.rb`

###Then some extra functionality for projects with cmock are in:

- `terrace_c.rb`
- `terrace_cmock.rb`


##Then there are a few example build configurations:

- `test_terrace.rb`
    Gives an example of how you would use just the core terrace features to build a project
- `test_terrace_c.rb`
    Gives an example setup of a C project that loads its configuration from a yml file
- `test_terrace_cmock.rb`
    Gives an example setup of C and CMock project
- `build_config.yml`
    This is a build configuration yml file used by the c and cmock examples.

## To run an example:

`rake -f test_terrace_cmock.rb`

That particular example will build the executable and then run all the tests.  A couple of the tests should fail :)

To clean you can do:

`rake -f test_terrace_cmock.rb clobber`

You can also do:

`rake -f test_terrace_cmock.rb test[substring]`

Which will run any tests that has a path containing the substring.
