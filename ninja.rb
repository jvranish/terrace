
require 'fileutils'

module Ninja
  class Error < StandardError; end

  class Ninja
    def initialize
      @ninja_file = ""
    end

    def add_subninjas(ninja_files)
      ninja_files.each do |ninja_file|
        @ninja_file += "subninja #{ninja_file}\n"
      end
    end

    def add_variables(variables)
      variables.each do |var_name, values|
        @ninja_file += "#{var_name} = #{values.join(" ")}\n"
      end
    end

    def add_rules(rules)
      rules.each do |name, command|
        @ninja_file += "rule #{name}\n"
        @ninja_file += "  command = #{command}\n"
      end
    end

    def add_default(targets)
      @ninja_file += "default #{targets.join(" ")}\n"
    end

    def add_edge(outputs, rule_name, inputs, vars = {}, implicit_deps = [])
      implicits = if !implicit_deps.empty? then " | #{implicit_deps.join(" ")}" else "" end
      @ninja_file += "build #{outputs.join(" ")}: #{rule_name} #{inputs.join(" ")}#{implicits}\n"
      vars.each do |var_name, value|
        @ninja_file += "  #{var_name} = #{value}\n"
      end
      return outputs
    end

    def add_edges(rule_name, inputs, &outfile_proc)
      inputs.flat_map do |i, implicit_deps|
        edge_info = outfile_proc.call(Pathname(i)).dup
        outfile = edge_info.delete(:out) do |field_name|
          raise Error.new("The proc for #{rule_name} did not result a #{field_name} field in its result hash")
        end
        add_edge([outfile], rule_name, ["#{i}"], edge_info, implicit_deps)
      end.to_a
    end

    def map_rule(rule, inputs, &outfile_proc)
      add_edges(rule, inputs.map{|i| [i, []]}, &outfile_proc)
    end

    def reduce_rule(rule, intputs, output)
      add_edge([output], rule, intputs)
    end

    def write_file(filename)
      FileUtils.mkpath(Pathname(filename).dirname)
      File.write(filename, @ninja_file)
    end

    def to_s
      @ninja_file
    end
  end
end

