
require 'rake'
require 'rake/clean'
require 'pathname'
require 'fileutils'
require 'thread'
require 'yaml'

require_relative 'ninja'

# advantages

# more flexible path support for headers/mocks (can support multiple files with the same name)
# much simpler
# much much faster
# much shorter no-op times (no super long rake -T like with ceedling)
# reports a failed test compile as a test failure (but will continue running tests)
# reports a test crash as a test failure (but will continue running tests)
# much more flexible build support
# support for using several different compilers/tools in the build process at the same time
# easy to allow multiple build configurations (debug/release, etc...)
# supports having multiple files with same name
# any flushed test output will get printed to the screen if test is killed

module Terrace

  class Error < StandardError; end

  def self.glob_warn_if_empty(g)
    Pathname.glob(g).tap{ |things|
      #TODO make this colored?
      puts "warning: glob #{g} did not expand to anything. Does it exist?" if things.empty?
    }
  end

  def self.expand_globs(globs)
    results = globs.flat_map{|g| self.glob_warn_if_empty(g)}.sort
  end

  def self.merge_config(config, other_config)
    config.merge(other_config) do |k, v_a, v_b|
      case v_a
      when Hash
        merge_config(v_a, v_b)
      else
        v_b
      end
    end
  end

  def self.module_path
    __FILE__
  end

  class BuildDir < Struct.new(:dir)
    def initialize(build_dir)
      super(Pathname(build_dir))
    end

    def in_dir(rel_path)
      dir + rel_path
    end

    def in_dir_sub_ext(rel_path, new_ext)
      in_dir(rel_path.sub_ext(new_ext))
    end

    def in_subdir_with_ext(subdir, rel_path, new_ext)
      dir + subdir + Pathname(rel_path).sub_ext(new_ext)
    end

    def to_s() dir.to_s end
  end
  
  class BuildConfig < Struct.new(:build_config)
    include Rake::DSL

    def self.read_yml(yml_path) YAML.load(File.read(yml_path)) end
    def self.load_yml(yml_path = 'build_config.yml')
      new(read_yml(yml_path))
    end

    def default_ninja
      if build_config.has_key?(:build_dir)
        build_config[:build_dir].in_dir("terrace.ninja")
      else
        "terrace.ninja"
      end
    end

    def build(ninja_file = nil, &block)
      ninja_file ||= default_ninja
      create_ninja(&block).write_file(ninja_file)
      sh "ninja -f #{ninja_file}"
    end

    def create_ninja(&block)
      ninja = Ninja::Ninja.new

      if build_config.has_key?(:build_dir)
        #this is a little hack that puts some of the ninja temporary
        # files in the build dir if it's specified
        ninja.add_variables({ builddir: ["#{build_config[:build_dir]}"] })
      end

      ninja.add_variables(build_config.fetch(:variables, {}))
      ninja.add_rules(build_config.fetch(:rules, {}))
      ninja.add_subninjas(build_config.fetch(:subninjas, []))

      block.call(ninja)
      return ninja
    end
  end
end
