require_relative 'terrace'

module TerraceC

  class CBuildDir < Terrace::BuildDir
    def object_file(src_file)  in_subdir_with_ext("out", src_file, ".o") end
    def dep_file(src_file)     in_subdir_with_ext("out", src_file, ".d") end

    def to_o_with_deps(src_file)
      { out: object_file(src_file),
        depfile: dep_file(src_file) }
    end

    def to_o(src_file)
      { out: object_file(src_file) }
    end
  end

  def self.module_path
    __FILE__
  end

  class CBuildConfig < Terrace::BuildConfig

    def self.load_config(build_config)
      new(expand_config(build_config))
    end

    def self.load_yml(yml_path = 'build_config.yml')
      load_config(read_yml(yml_path))
    end

    def self.expand_config(build_config)
      inc_dirs = Terrace::expand_globs(build_config[:inc_dirs]).to_a
      enhanced_config = {
        c_files: Terrace::expand_globs(build_config[:c_files]),
        inc_dirs: inc_dirs,
        build_dir: CBuildDir.new(build_config[:build_dir]),
        variables: {
          inc_dir_args: inc_dirs_args(inc_dirs)
        },
      }
      return Terrace::merge_config(build_config, enhanced_config)
    end

    def self.load_project(yml_path = 'build_config.yml')
      load_yml(yml_path).tap do |build_config|
        build_config.create_build_task
      end
    end

    def self.inc_dirs_args(dirs) dirs.map{|d| "-I#{d}"}.to_a end

    def build_target() Pathname(build_config[:build_target]) end
    def c_files()      build_config[:c_files] end
    def build_dir()    build_config[:build_dir] end
    def inc_dirs()     build_config[:inc_dirs] end

    def default_ninja
      build_dir.in_dir_sub_ext(build_target, ".ninja")
    end

    def create_build_task(task_name = :build)
      CLOBBER.include(build_dir.to_s)
      
      task task_name do
        build do |ninja|
          o_files = ninja.map_rule(:cc, c_files) do |c_file|
            build_dir.to_o_with_deps(c_file)
          end
          elf_file = ninja.reduce_rule(:ld, o_files, build_dir.in_dir(build_target))
          ninja.add_default(elf_file)
        end
      end

    end
  end

end
