require_relative 'tools/CMock/lib/cmock'
require_relative 'tools/CMock/vendor/unity/auto/generate_test_runner'
require_relative 'tools/CMock/vendor/unity/auto/unity_test_summary.rb'

require_relative 'terrace_c'

module TerraceCMock

  class AmbiguousMatchForInclude < Terrace::Error; end
  class TestFailures < Terrace::Error; end

  class TestDir < TerraceC::CBuildDir
    attr_reader :mock_prefix
    def initialize(test_dir, mock_prefix = "Mock")
      super(test_dir)
      @mock_prefix = mock_prefix
    end

    def test_bin(test_file)    in_subdir_with_ext("bin", test_file, "") end
    def test_result(test_file) in_subdir_with_ext("results", test_file, ".test_result") end
    def ninja_file(test_file)  in_subdir_with_ext("ninja", test_file, ".ninja") end
    def test_runner(test_file) in_subdir_with_ext("runners", test_file, "_runner.c") end
    def mock_file(rel_path)    dir + "mocks" + rel_path end
    def tests_ninja_file()     in_dir("tests.ninja") end

    def mocks_corresponding_to_header(h)
      dirname, basename = h.split
      mock_h = mock_file(dirname + "#{@mock_prefix}#{basename}")
      mock_c = mock_h.sub_ext(".c")
      return [mock_h, mock_c]
    end

    def mock_outputs(header)
      dirname, base = (dir + "mocks" + Pathname(header).sub_ext("")).split
      mock_header = dirname + "#{@mock_prefix}#{base}.h"
      mock_source = dirname + "#{@mock_prefix}#{base}.c"
      { out: [mock_header, mock_source],
        out_dir: dirname }
    end

    def to_test_runner(test_file)
      { out: test_runner(test_file) }
    end
  end

  def self.extract_includes(filename)
    File.readlines(filename).map {|line|
      line.match(/^\s*#include\s+\"\s*(.+\.[hH])\s*\"/)
    }.compact.map{|m| m[1]}
  end

  def self.tails(xs)
    if xs.empty?
      []
    else
      [xs] + tails(xs.drop(1))
    end
  end

  def self.path_tails(path)
    tails(path.each_filename.to_a).map{|xs| File.join(*xs)}
  end

  def self.lookup_source(lookup_table, h_key)
    s = Pathname(h_key).sub_ext(".c").to_s
    case lookup_table[s].length
    when 0
      nil
    when 1
      lookup_table[s].first
    else
      #TODO also add a test for this case
      #TODO colorize
      raise AmbiguousMatchForInclude.new("error: #{h_key} could refer to multiple sources: #{lookup_table[s]}")
    end
  end

  def self.headers_in_incdir(d)
    Terrace::glob_warn_if_empty(d + "**/*.h")
  end

  
 
  #TODO fix this method name
  def self.module_path
    __FILE__
  end

  class CMockBuildConfig < TerraceC::CBuildConfig
    include Rake::DSL

    CMOCK_AND_UNITY_C_FILES = [
      "tools/CMock/vendor/unity/src/unity.c",
      "tools/CMock/src/cmock.c",
    ]
    CMOCK_AND_UNITY_INCLUDE_DIRS = [
      "tools/CMock/vendor/unity/src",
      "tools/CMock/src",
      "tools/CMock/vendor/unity/examples/example_3/helper",
    ]

    def initialize(build_config)
      super(build_config)
      @mutex = Mutex.new
    end

    def self.expand_config(config)
      build_config = super(config.merge({ inc_dirs: config[:inc_dirs] + CMOCK_AND_UNITY_INCLUDE_DIRS}))
      inc_dirs = build_config[:inc_dirs]
      test_dir = TestDir.new(build_config[:test_dir],
                              build_config[:mock_prefix])
      enhanced_config = {
        test_files: Terrace::expand_globs(build_config[:test_files]),
        build_dir: test_dir,
        variables: {
          mock_inc_dir_args: mock_inc_dirs_args(test_dir, inc_dirs)
        },
        rules: {
          mock: "ruby tools/CMock/lib/cmock.rb --verbosity=1 --mock_path=$out_dir $in", # $out is a directory in this case
          generate_test_runner: "ruby tools/CMock/vendor/unity/auto/generate_test_runner.rb $in $out",
          test_cc: "gcc -c $in -o $out $inc_dir_args $mock_inc_dir_args $c_flags $test_c_flags -MMD -MF $depfile", 
          test_ld: "gcc $in -o $out $test_ld_flags $ld_flags",
        }
      }
      return Terrace::merge_config(build_config, enhanced_config)
    end

    def self.load_project(yml_path = 'build_config.yml')
      load_yml(yml_path).tap do |build_config|
        build_config.create_test_task([Terrace::module_path,
                                       TerraceC::module_path,
                                       TerraceCMock::module_path,
                                       yml_path])
      end
    end

    def create_test_task(other_config_files = [], task_name = :test)
      CLOBBER.include(test_dir.to_s)
      create_cmock_tasks(other_config_files)
      build_test_object_files_ninja
    end

    def self.mock_inc_dirs_args(test_dir, dirs)
      dirs.map{|d| "-I#{test_dir.mock_file(d)}" }
    end

    def test_inc_dirs() build_config[:inc_dirs] end 
    def test_files()  build_config[:test_files] end
    def test_dir()    build_config[:build_dir] end

    def run_ninja(ninja_file, targets = [])
      sh "ninja -f #{ninja_file} #{targets.join(" ")}"
    end

    def write_file(filename, contents)
      FileUtils.mkpath(Pathname(filename).dirname)
      File.write(filename, contents)
    end

    def print(txt)
      @mutex.synchronize do
        puts txt
      end
    end

    #This is complicated because it will buffer output so that the output is sane 
    # when the tests are run in parallel, but it will _also_ actually flush any 
    # accumulated output if interrupted (say with ctrl+c)
    def run_test(test_bin, test_result_filepath)
      output = ""
      begin
        IO.popen("#{test_bin}") do |io|
          while b = io.read(1)
            output += b
          end
        end

        lines = output.lines
        if lines.nil? || !(lines[-1].strip == "OK" || lines[-1].strip == "FAIL")
          output += "\n" +
            "-----------------------\n" +
            "#{test_file}:0:Run:FAIL: Unexpected test output (it didn't end in OK or FAIL)\n" +
            "-----------------------\n" +
            "1 Tests 1 Failures 0 Ignored\n" +
            "FAIL\n"
        end
        write_file(test_result_filepath, output)
      ensure
        print(output)
      end
    end

    def summarize_test_results(test_results)
      uts = UnityTestSummary.new
      uts.set_targets(test_results)
      print(uts.run)
      if (uts.failures > 0)
        raise TestFailures.new("FAIL: There were unit test failures")
      end
    end


    def build_test_object_files_ninja
      Terrace::BuildConfig.new(build_config).create_ninja do |n|

        test_runners = n.map_rule(:generate_test_runner, test_files, &test_dir.method(:to_test_runner))
        header_files = header_hash.values.uniq
        mocks = n.map_rule(:mock, header_files, &test_dir.method(:mock_outputs))
        
        test_c_files = c_files + mocks.map{|h, c| c} + CMOCK_AND_UNITY_C_FILES
        test_o_files = n.map_rule(:test_cc, test_c_files, &test_dir.method(:to_o_with_deps))

        n.add_subninjas(test_files.map(&test_dir.method(:ninja_file)))
        n.add_default(test_files.map(&test_dir.method(:test_bin)))

      end.write_file(test_dir.tests_ninja_file)
    end

    def header_hash
      Hash[inc_dirs.flat_map{ |d|
        TerraceCMock::headers_in_incdir(d).map { |h|
          [h.relative_path_from(d).to_s, h]
        }
      }]
    end

    def mock_hash
      lookup_table = Hash.new { |hash, key| hash[key] = [] }
      c_files.each do |src|
        TerraceCMock::path_tails(src).each do |t|
          lookup_table[t] += [src]
        end
      end

      Hash[header_hash.map do |h_key, h_path|
        mock_h_path, mock_c_path = test_dir.mocks_corresponding_to_header(h_path)
        [h_key, {
          header_path: h_path,
          mock_h_path: mock_h_path,
          mock_c_path: mock_c_path,
          #It's valid for source_path to nil if there isn't a corresponding source file
          source_path: TerraceCMock::lookup_source(lookup_table, h_key),
        }]
      end]
    end

    def test_file_dependencies(test_file, mock_prefix)
      explicit_deps = []
      implicit_deps = []
      TerraceCMock::extract_includes(test_file).each do |h|
        dirname, header_base = Pathname(h).split
        m = header_base.to_s.match("^#{mock_prefix}(.*)")
        if m # if this is a mock include, depend on the mock source
          h_name = (dirname + m[1]).to_s
          x = mock_hash.fetch(h_name, {})
          explicit_deps << x[:mock_c_path]
          implicit_deps << x[:mock_h_path]
        else # if this is a normal include, depend on the real source
          explicit_deps << mock_hash.fetch(h, {})[:source_path]
        end
      end
      return [explicit_deps.compact, implicit_deps.compact] # remove nils
    end

    def create_cmock_tasks(other_config_files)

      test_results = test_files.map do |test_file|
        ninja_file = test_dir.ninja_file(test_file)
        test_bin = test_dir.test_bin(test_file)
        test_result = test_dir.test_result(test_file)

        file ninja_file => [test_file] + other_config_files do
          generate_test_ninja_file(ninja_file, test_file, test_bin)
        end

        file test_result => test_bin do
          run_test(test_bin, test_result)
        end

        test_result
      end

      result_summary = test_dir.in_dir("test_results_summary.txt")

      task :build_tests => test_files.map(&test_dir.method(:ninja_file)) do
        run_ninja(test_dir.tests_ninja_file)
      end
      multitask :_all_results => test_results #this task should _only_ be invoked after :build_tests
      task :run_tests => [:build_tests, :_all_results]    
      
      task :test, [:test_substring] do |t, args|
        if args[:test_substring].nil?
          # if test task invoked without args, build everything in parallel
          #   also test output is buffered (to make the interleaved output sane)
          Rake::Task[:run_tests].invoke
          summarize_test_results(test_results.map{|p| p.to_s})
        else
          # if test task invoke with argument, we filter tests for any that match the substring,
          #  and then run them one at a time, unbuffered output
          results = test_files.select{|f| f.to_s.include?(args[:test_substring])}.map do |test_file|
            ninja_file = test_dir.ninja_file(test_file)
            test_bin = test_dir.test_bin(test_file)
          
            Rake::Task[ninja_file].invoke
            #TODO merge this with run_ninja() somehow? this does need to still
            system("ninja -f #{test_dir.tests_ninja_file} #{test_bin}") and system("#{test_bin}")
          end
          raise TestFailures.new if !results.all?
          #TODO add colorization here
          print("All tests PASSED!")
        end
      end
    end

    def generate_test_ninja_file(ninja_file, test_file, test_bin)
      sources_to_link_in, implicit_header_deps = 
        test_file_dependencies(test_file, test_dir.mock_prefix)
      
      #build test ninja file
      ninja = Ninja::Ninja.new
      test_file_sources = [[test_file, implicit_header_deps],
                          [test_dir.test_runner(test_file), implicit_header_deps]]
      o_files = ninja.add_edges(:test_cc, test_file_sources, &test_dir.method(:to_o_with_deps))
      other_o_files = (sources_to_link_in +
                       CMOCK_AND_UNITY_C_FILES).map(&test_dir.method(:object_file))
      ninja.add_edge(["#{test_bin}"], :test_ld, o_files + other_o_files)
      ninja.write_file(ninja_file)
    end

  end

  #TODO add ninja escaping to build lines, etc..
  #TODO add rake task to run specific test
  #TODO add color reporting
  #TODO colorize test output
  #TODO colorize test summary
  #TODO colorize build system outputs
  #TODO require cmock for mock generation instead of shelling out
end

