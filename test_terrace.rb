require_relative 'terrace'

c_files = Terrace::expand_globs(["src/**/*.c"])
inc_dirs = Terrace::expand_globs(["src/**/"])

build_target = "my_test_build"

build_dir = Terrace::BuildDir.new("build")

CLOBBER.include(build_dir.to_s)

task :build do
  Terrace::BuildConfig.new(
    build_dir: build_dir,
    variables: {
      c_flags:
        [ "-fcolor-diagnostics",
          "-std=c99",
        ] + inc_dirs.map{|d| "-I#{d}"},
      ld_flags: ["-lm"]
    },
    rules: {
      cc: "gcc -c $in -o $out $c_flags -MMD -MF $depfile",
      ld: "gcc $in -o $out $ld_flags"
    }).build do |ninja|
      o_files = ninja.map_rule(:cc, c_files) do |c_file|
        { out: build_dir.in_subdir_with_ext("out", c_file, ".o"),
          depfile: build_dir.in_subdir_with_ext("out", c_file, ".d") }
      end

      elf_file = ninja.reduce_rule(:ld, o_files, build_dir.in_dir(build_target))
  end
end

task :default => [:build]

