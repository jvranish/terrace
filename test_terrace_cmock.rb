require_relative 'terrace_cmock'

TerraceC::CBuildConfig.load_project("build_config.yml")
TerraceCMock::CMockBuildConfig.load_project("build_config.yml")

task :default => [:build, :test]

